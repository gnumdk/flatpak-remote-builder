#!/bin/bash

function get_base_command
{
    app_id=$(jq '."app-id"' "$1" | sed 's/"//g')
    path="$(jq  '."build-options"."append-path"' "$1" 2>/dev/null| sed 's/"//g')"
    if [[ "$path" != "null" ]]
    then
        path="/app/bin:/usr/bin:$path"
    else
        path="/app/bin:/usr/bin"
    fi
    build_options=$(jq  '."build-options"."build-args"[]' "$1" 2>/dev/null| sed 's/"//g')
    command="flatpak build --env=TERM=xterm-256color"
    command="$command --env=CCACHE_DIR=$HOME/.cache/flatpak-remote-builder/ccache"
    command="$command --env=PATH=$path --env=G_MESSAGES_DEBUG="
    command="$command --env=XDG_RUNTIME_DIR=/run/user/$UID"
    command="$command --build-dir=$HOME/.cache/flatpak-remote-builder/builds/$app_id"
    if [[ "$build_options" != "" ]]
    then
        command="$command $build_options"
    fi
    command="$command --filesystem=host"
    command="$command --filesystem=$HOME/.cache/flatpak-remote-builder"
    command="$command --filesystem=$2 --env=RUST_LOG={{name}}=debug"
    command="$command --env=RUST_BACKTRACE=1 --env=NOCONFIGURE=1"
    command="$command $HOME/.cache/flatpak-remote-builder/$app_id"
    mkdir -p $HOME/.cache/flatpak-remote-builder/builds/$app_id
    echo $command
}

function configure
{
    get_base_command "$1" "$2"
    command=$(get_base_command "$1" "$2")
    command="$command meson $2 $HOME/.cache/flatpak-remote-builder/builds/$app_id"
    command="$command --prefix /app -Dprofile=development"
    $command
    if (( $? != 0 ))
    then
        exit -1
    fi
}

function make
{
    command=$(get_base_command "$1" "$2")
    command="$command ninja"
    $command
    if (( $? != 0 ))
    then
        exit -1
    fi
}

function make_install
{
    command=$(get_base_command "$1" "$2")
    command="$command ninja install"
    $command
    if (( $? != 0 ))
    then
        exit -1
    fi
}

function get_commands
{
    typeset -f get_base_command configure make make_test make_install
}