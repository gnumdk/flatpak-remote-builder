#!/bin/bash

function run_app
{

    app_id=$(jq '."app-id"' "$1" | sed 's/"//g')
    app_cmd=$(jq .command "$1" | sed 's/"//g')
    finish_args=$(jq  '."finish-args"[]' "$1"| sed 's/"//g'|grep -v metadata|tr '\n' ' ')
    command="flatpak build --with-appdir --allow=devel"
    command="$command --bind-mount=/run/user/1000/doc=/run/user/1000/doc/by-app/$app_id"
    command="$command --bind-mount=/run/host/fonts=/usr/share/fonts"
    [[ -e /var/cache/fontconfig ]] && command="$command --bind-mount=/run/host/fonts-cache=/var/cache/fontconfig"
    command="$command --filesystem=/home/gnumdk/.local/share/fonts:ro"
    command="$command --filesystem=$HOME/.cache/fontconfig:ro"
    command="$command --bind-mount=/run/host/user-fonts-cache=$HOME/.cache/fontconfig"
    command="$command --bind-mount=/run/host/font-dirs.xml=$HOME/.cache/font-dirs.xml"
    command="$command --bind-mount=/run/host/font-dirs.xml=$HOME/.cache/font-dirs.xml"
    command="$command --talk-name=org.freedesktop.portal.* --talk-name=org.a11y.Bus"
    command="$command --filesystem=/home/gnumdk/.cache/flatpak-remote-builder/$app_id"
    command="$command --env=DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/$UID/bus"
    command="$command --env=DESKTOP_SESSION=gnome --env=DISPLAY=:0 --env=SHELL=/bin/bash"
    command="$command --env=SSH_AUTH_SOCK=$SSH_AUTH_SOCK --env=USER=$USER"
    command="$command --env=WAYLAND_DISPLAY=wayland-0 --env=XDG_CURRENT_DESKTOP=GNOME"
    command="$command --env=XDG_MENU_PREFIX=gnome- --env=XDG_SESSION_DESKTOP=gnome"
    command="$command --env=XDG_SESSION_TYPE=wayland --env=TERM=xterm-256color"
    command="$command $finish_args"
    command="$command /home/gnumdk/.cache/flatpak-remote-builder/$app_id $app_cmd"
    $command
}

function build_init
{
    app_id=$(jq '."app-id"' "$1" | sed 's/"//g')
    sdk=$(jq .sdk "$1" | sed 's/"//g')
    sdk_extensions=$(jq  '."sdk-extensions"[]' "$1" 2>/dev/null| sed 's/"//g')
    runtime=$(jq .runtime "$1" | sed 's/"//g')
    runtime_version=$(jq '."runtime-version"' "$1" | sed 's/"//g')
    command="flatpak build-init --type=app --arch=x86_64"
    command="$command $HOME/.cache/flatpak-remote-builder/$app_id $app_id"
    command="$command $sdk $runtime $runtime_version"
    for extension in $sdk_extensions
    do
        command="$command --sdk-extension=$extension"
    done
    $command
    if (( $? != 0 ))
    then
        exit $?
    fi
}

function download_deps
{
    app_id=$(jq '."app-id"' "$1" | sed 's/"//g')
    app_cmd=$(jq .command "$1" | sed 's/"//g')
    command="flatpak-builder --arch=x86_64 --ccache --force-clean"
    command="$command --state-dir $HOME/.cache/flatpak-remote-builder/state"
    command="$command --download-only --disable-updates --stop-at=$app_cmd"
    command="$command $HOME/.cache/flatpak-remote-builder/$app_id"
    $command "$1"
    if (( $? != 0 ))
    then
        exit $?
    fi
}

function build_deps
{
    app_id=$(jq '."app-id"' "$1" | sed 's/"//g')
    app_cmd=$(jq .command "$1" | sed 's/"//g')
    command="flatpak-builder --arch=x86_64 --ccache --force-clean"
    command="$command --state-dir $HOME/.cache/flatpak-remote-builder/state"
    command="$command --disable-download --disable-updates --stop-at=$app_cmd"
    command="$command $HOME/.cache/flatpak-remote-builder/$app_id"
    $command "$1"
    if (( $? != 0 ))
    then
        exit $?
    fi
}