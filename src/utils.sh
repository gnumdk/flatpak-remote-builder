#!/bin/bash

BUILD=$(( 1 << 1 ))
COMPILE=$(( 1 << 2 ))
RUN=$(( 1 << 3 ))
CLEAN=$(( 1 << 4 ))

function usage
{
    echo "usage: $0 [build|compile|run|clean]"
    echo "  SRCDIR should be set to project directory"
}

function flags_from_cmdline
{
    flags=0
    for arg in $@
    do
        if [[ "$arg" == "build" ]]
        then
            flags=$(( flags | $BUILD ))
        elif [[ "$arg" == "compile" ]]
        then
            flags=$(( flags | $COMPILE ))
        elif [[ "$arg" == "run" ]]
        then
            flags=$(( flags | $RUN ))
        elif [[ "$arg" == "clean" ]]
        then
            flags=$(( flags | $CLEAN ))
        fi
    done
    echo $flags
}

function check_bin
{
    for bin in jq flatpak rsync nc
    do
        whereis $bin >/dev/null 2>&1
        if (( $? != 0 ))
        then
            echo "$bin missing!"
            exit -1
        fi
    done
}

function check_conf
{
    if [[ "$HOST" == "" ]]
    then
        echo "HOST missing" >&2
        echo 0
    elif [[ "$USER" == "" ]]
    then
        echo "USER missing" >&2
        echo 0
    else
        echo 1
    fi
}

function get_json
{
    find $SRCDIR -name *.json | while read file
    do
        grep app-id $file >/dev/null
        if (( $? == 0 ))
        then
            echo $file
            return
        fi
    done
    exit -1
}