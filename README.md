
This is a BASH script allowing you to build applications remotely via SSH.

I was tired of my laptop getting hot every time I clicked the build button in GNOME Builder.

You need to set a username/hostname in /usr/local/etc/flatpak-remote-builder.conf like this:
```
 # Mandatory:
 # Host used for bulding: needs a SSH connexion with pubkey auth and SSH agent
 HOST=hostname.org
 # User connecting to HOST
 USER=my_username

 # Optional:
 # SSH authentication sock, GNOME keyring default value here
 SSH_AUTH_SOCK=/run/user/$UID/keyring/ssh
```

**Warning**

Only meson is handled but feel free to contribute

**Usage**

SRCDIR should be set to project directory. (Done by GNOME Builder external commands)

* Build
```
 flatpak-remote-builder build
```

* Clean
```
 flatpak-remote-builder clean
```

Example in GNOME Builder:

![](https://i.imgur.com/QhztwCV.png)
